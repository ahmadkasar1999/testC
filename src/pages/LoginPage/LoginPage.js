import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Container, Grid } from 'semantic-ui-react';
import { useSelector } from 'react-redux';

import FormLogin from '../../components/Form/FormLogin';
function LoginPage() {
  const user = useSelector((state) => state.user);
  const { userInfo } = user;
  let history = useHistory();
  useEffect(() => {
    if (userInfo) {
      history.push('/');
    }
  }, [history, userInfo]);
  return (
    <Grid style={{ width: '100% ', display: 'flex', justifyContent: 'center' }}>
      <Grid.Row>
        <Grid.Column mobile={16} tablet={10} computer={10}>
          <Container>
            <FormLogin />
          </Container>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
export default LoginPage;

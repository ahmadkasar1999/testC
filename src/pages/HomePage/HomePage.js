import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import './HomePage.css';
function HomePage() {
  const user = useSelector((state) => state.user);
  const { userInfo } = user;
  let history = useHistory();
  useEffect(() => {
    if (!userInfo) {
      history.push('/login');
    } else {
      toast.success(`welcome ${userInfo.userName}`, {
        position: 'top-right',
        autoClose: 3000,
      });
    }
  }, [history, userInfo, user.userName]);
  return (
    <div className='homeContainer'>
      <img src={window.location.origin + '/image/welcome.jpg'} alt='not found' height='100%' width='100%' />
      <ToastContainer position='top-right' autoClose={500} hideProgressBar={false} newestOnTop={false} closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover />
    </div>
  );
}

export default HomePage;

import React from 'react';
import { Button, Checkbox, Form, Input, Card } from 'semantic-ui-react';
import useFormLogin from './useFormLogin';
import './Form.css';
function FormLogin() {
  const { password, email, emailEr, loading, passwordEr, onSubmitHandler, checkErrorPassword, checkErrorEmail } = useFormLogin();
  return (
    <Card className='cardForm'>
      <Card.Content>
        <Form className='login' onSubmit={onSubmitHandler} loading={loading}>
          <Form.Field
            label='Email'
            type='email'
            control={Input}
            required
            error={!emailEr ? { content: 'Please enter a valid email address' } : false}
            onChange={(e) => checkErrorEmail(e.target.value)}
          />
          <Form.Field
            label='Password'
            type='password'
            control={Input}
            required
            error={!passwordEr ? { content: 'password length must be at lease 6 ' } : false}
            onChange={(e) => checkErrorPassword(e.target.value)}
          />
          <Form.Field>
            <Checkbox className='check' label='I agree to the Terms and Conditions' />
          </Form.Field>
          <div className='buttonContainer'>
            <Button type='submit' className='buttonLogin' disabled={email !== '' && password !== '' ? false : true}>
              Submit
            </Button>
          </div>
        </Form>
      </Card.Content>
    </Card>
  );
}
export default FormLogin;

import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loginUser } from '../../redux/actions/userActions';
const useFormLogin = () => {
  const user = useSelector((state) => state.user);
  const { loading } = user;
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailEr, setEmailEr] = useState(true);
  const [passwordEr, setPasswordEr] = useState(true);
  const checkErrorEmail = (e) => {
    if (e.length < 3) {
      setEmailEr(false);
      setEmail('');
    } else {
      setEmailEr(true);
      setEmail(e);
    }
  };
  const checkErrorPassword = (e) => {
    if (e.length < 6) {
      setPasswordEr(false);
      setPassword('');
    } else {
      setPasswordEr(true);
      setPassword(e);
    }
  };
  const onSubmitHandler = (e) => {
    e.preventDefault();
    dispatch(loginUser(email, password));
  };
  return {
    password,
    email,
    emailEr,
    loading,
    passwordEr,
    onSubmitHandler,
    checkErrorPassword,
    checkErrorEmail,
  };
};

export default useFormLogin;

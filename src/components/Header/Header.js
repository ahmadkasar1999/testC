import React from 'react';
import { Header } from 'semantic-ui-react';
import Navbar from './Navbar/Navbar';
function Headers() {
  return (
    <Header className='head'>
      <Navbar />
    </Header>
  );
}

export default Headers;

import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../../redux/actions/userActions';

const useNavbar = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const user = useSelector((state) => state.user);
  const { userInfo } = user;
  const handleClick = () => {
    setOpen(!open);
  };

  const closeMenu = () => {
    setOpen(false);
  };
  const logoutUser = () => {
    dispatch(logout());
    history.push('/login');
  };
  return {
    open,
    userInfo,
    user,
    handleClick,
    closeMenu,
    logoutUser,
  };
};

export default useNavbar;

import React from 'react';
import { FiMenu, FiX } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import './Navbar.css';
import useNavbar from './useNavbar';
const Navbar = () => {
  const { open, user, userInfo, handleClick, closeMenu, logoutUser } = useNavbar();
  return (
    <nav className='navbar'>
      <Link to='/' className='nav-logo'>
        Logo
      </Link>
      <div onClick={handleClick} className='nav-icon'>
        {open ? <FiX /> : <FiMenu />}
      </div>
      <ul className={open ? 'nav-links active' : 'nav-links'}>
        <li className='nav-item'>
          <Link to='/' className='nav-link' onClick={closeMenu}>
            Home
          </Link>
        </li>
        {user && userInfo ? (
          <li className='nav-item'>
            <section className='ui bottom menu profile'>
              <div className='ui simple dropdown item'>
                {user && userInfo ? userInfo.userName : ''}
                <i className='dropdown icon'></i>
                <div className='menu' style={{ zIndex: '1100' }}>
                  {user && userInfo ? (
                    <>
                      <p className='item' onClick={() => logoutUser()}>
                        Logout
                      </p>
                    </>
                  ) : (
                    ''
                  )}
                </div>
              </div>
            </section>
          </li>
        ) : (
          ''
        )}
      </ul>
    </nav>
  );
};

export default Navbar;

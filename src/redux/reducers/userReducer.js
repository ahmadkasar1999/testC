import { LoginTypes } from '../constans/action-type';

const userInfoFromLocalStorage = localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : null;

const initialState = {
  userInfo: userInfoFromLocalStorage,
  loading: false,
  error: null,
  success: false,
};

export const userLoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LoginTypes.USER_LOGIN_REQ:
      return { ...state, loading: true };
    case LoginTypes.USER_LOGIN_SUCCESS:
      return { ...state, loading: false, userInfo: action.payload, success: true };
    case LoginTypes.USER_LOGOUT:
      return {};
    default:
      return state;
  }
};

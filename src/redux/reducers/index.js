import { combineReducers } from 'redux';
import { userLoginReducer } from './userReducer';

const reducers = combineReducers({
  user: userLoginReducer,
});

export default reducers;

# Simple React JS Project

## What is the use of this Repo

This Project is a Simple ReactJS Project which demonstrates the following

1. Creating a Component in React
2. Making Custom Hook
3. Communicating between parent and child component
4. Using Semantic UI with React
5. Using Basic Routing in React

## Prerequisites

### Install Node JS

Refer to https://nodejs.org/en/ to install nodejs

### Install create-react-app

Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app

```bash
npm install -g create-react-app
```

## Live Application URL

The Application is deployed in https://pensive-brattain-1d2ca6.netlify.app

Click on the link to see the application

## Cloning and Running the Application in local

Clone the project into local

Install all the npm packages. Go into the project folder and type the following command to install all npm packages

```bash
npm install
```

In order to run the application Type the following command

```bash
npm start
```

The Application Runs on **localhost:3000**

### components

1. **Form/FormLogin** Component : This Component displays a form of input fields (Email filed and password filed).

2. **Form/useFormLogin** Component : This Component contains a login of **Form/FormLogin** Component.

3. **Header/Navbar/Navbar** Component : This Component displays the links of the project.

4. **Header/Navbar/useNavbar** Component : This Component contains a login of **Header/Navbar/Navbar** Component.

### pages

1. **HomePage** Page: This Page displays a simple jsx code and notification with useName when user login

2. **LoginPage** Page: This Page displays a **Form/useFormLogin** Component

### packages

**react-router-dom** is the most popular library for implementing routing for React apps.

**axios** library is used to make HTTP Calls

**react-toastify** library is used to add notifications to our apps with ease

**redux** library for managing and centralizing application state

**semantic-ui-react** a front-end development framework is used to provide React components,it helps create beautiful and responsive layouts
